REST service to detect visages
===============================

DEPENDENCIES
------------
1. Clone the repository and move to the project's directory.

2. Install python-dev to be able to use tensorflow
	```
	$ sudo apt-get install python-dev
	```
3. If you don't have virtualenv installed, install it
	```
	$ sudo apt-get install virtualenv
	```
4. Create a virtual environment to make sure that you don't damage other python projects.
	```
	$ virtualenv venv
	(venv)$ source venv/bin/activate
	```
5. To install Python dependencies, run
	```
	(venv)$ pip install -r requirements.txt
	```
6. The requirements file download tensorflow for linux 64 bits. If it does not match your configuration, please visit the page: https://www.tensorflow.org/versions/r0.10/get_started/os_setup.html

HOW TO USE
----------
1. Download the weights and biais and save them in the project main folder, in a folder called **model** (it can take a couple of minutes)

	https://mega.nz/#F!XABElSDa!uFMs1WAzIjYzzTr1umHC2g

2. Run: python server.py [port_number] and wait for the model to be restored (it can take a couple of seconds)

3. Go to the address provided by the server (it will look like: http://0.0.0.0:8080/)

4. Choose an image on the web and put its url in the input box

5. Submit

Note : 

If there is an error while launching the server, you may need to precise an available port (ex: python server.py 3030)

DESCRIPTION
-----------

Databases uses images from **ImageNet** and **Labeled Faces in the Wild - University of Massachusetts**

The architecture of the CNN is **VGG 16** and uses **Tensorflow**

Pre-trained weights (not last layer) were taken from : https://github.com/machrisaa/tensorflow-vgg