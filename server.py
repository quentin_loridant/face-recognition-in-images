#--------------------------------------------------------
#                Face detection project
#--------------------------------------------------------
# File:         server.py
# Author:       Quentin Loridant
# Date:         September 2016
# Description:  Server for face detection REST API

import web
from web import form
import urllib
import numpy as np
from PIL import Image
import evaluation as ev

# Load the model to the server
m = ev.Model()

render = web.template.render('web/templates/')

urls = ('/', 'index')
app = web.application(urls, globals())

# Creating a form to fetch images URL
myform = form.Form( 
    form.Textbox("ImageURL"))

class index: 
    def GET(self): 
        form = myform()
        return render.index(form)

    def POST(self): 
        form = myform() 
        if not form.validates(): 
            return render.index(form)
        else:
        	# form.d.boe and form['boe'].value are equivalent ways of
            # extracting the validated arguments from the form.
            urllib.urlretrieve(form.d.ImageURL, "web/imageToEvaluate.jpg")
            url = form.d.ImageURL
            face = m.evaluateImage("web/imageToEvaluate.jpg")
            return render.image(url, face)

if __name__=="__main__":
    web.internalerror = web.debugerror
    app.run()