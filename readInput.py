#--------------------------------------------------------
#                Face detection project
#--------------------------------------------------------
# File:			readInput.py
# Author:       Quentin Loridant
# Date:         September 2016
# Description:  Prepare the input data to train the vgg16 architecture. The data shoud be split into 2 folders:
# 				- One containing the data with faces. Its path should be provided as the FIRST argument when running this file
#				- The other containing pictures without faces. Its path should be provided as the SECOND argument when running this file
# This file writes in a random order the names of all the files with its class, in "test-set.py". This file will be read while training the vgg16 network.

from glob import glob
import numpy as np

# Loading the images filenames
face_names = glob("/home/quentin/workspace/data-set/faces/*")
face_arr = np.ones(len(face_names))

no_face_names = glob("/home/quentin/workspace/data-set/not-faces/*")
no_face_arr = np.zeros(len(no_face_names))

# Adding faces and non faces filenmaes
names = np.array(np.concatenate([face_names,no_face_names]))
arr = np.array(np.concatenate([face_arr,no_face_arr]))

inds = range(names.shape[0])
np.random.shuffle(inds)

batch = np.array([names, arr])

# Save the filenames stored in random order
np.save("test-set", batch.T[inds])
