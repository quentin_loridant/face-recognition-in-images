#--------------------------------------------------------
#                Face detection project
#--------------------------------------------------------
# File:			training.py
# Author:       Quentin Loridant
# Date:         September 2016
# Description:  Train the vgg16 architecture. The data should be already set. If not, see the file "readInput.py"
#				The training contains 20 000 batches. Each batch is made of 8 images. The images filenames are selected 
#				in the "test-set.py" file. The actual images are then downloaded in their respective folder. No more than
#				8 images are downloaded at the same time.
#			
#				The path containing the images with faces should be provided as the FIRST argument when running this file
#				The path containing the images without face should be provided as the SECOND argument when running this file

import numpy as np
import tensorflow as tf 
import vgg16
from PIL import Image

# Size of the input images
size = (224, 224)

folder_path_faces = "/home/quentin/workspace/data-set/faces/"
folder_path_no_faces = "/home/quentin/workspace/data-set/not-faces/"

# Load all the filenames of the umages
arr = np.load("test-set.npy")

def load_batch(j, size):
	image_batch = np.array([np.array(Image.open(a).convert("RGB").resize([224,224])) for a in arr[j:(j+size),0].tolist()])
	label_batch = np.zeros([size, 2])
	for i in range(size):
		label_batch[i,arr[j+i,1].astype('float')] = 1
	return image_batch, label_batch


# Connection with the backend
sess = tf.InteractiveSession() 

# Nodes for the input images (RGB with 224*224 resolution)
image_input = tf.placeholder(tf.float32, [None, 224, 224, 3], name="input")
# Target for the output classes
y_ = tf.placeholder(tf.float32, shape=[None, 2], name="output")

# # Calling the vgg16 architecture
network = vgg16.Vgg16("vgg16.npy")
y = tf.clip_by_value(network.build(image_input), 1e-15, 1)

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y)))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.initialize_all_variables())

# Batch size, usually a divider of 2
bs = 8 

# Creation of a saver for the model
saver = tf.train.Saver()
j = 0

for i in range(20000):
	if j < arr.shape[0] - bs:
		batchImages, batchClasses = load_batch(j, bs)
		j = j + bs
	else:
		batchImages, batchClasses = load_batch(j, arr.shape[0]-j - 1)
	  	j = 0

	train_accuracy, pred, _ =sess.run([accuracy, y, train_step], feed_dict={image_input:batchImages, y_: batchClasses})
	print("step %d, training accuracy %g"%(i, train_accuracy))
	print pred
	print batchClasses
	if (i%100 == 0):
		saver.save(sess, 'model/model.ckpt', global_step=0)
		print("[INFO] Model saved")

