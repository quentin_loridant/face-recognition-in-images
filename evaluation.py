#--------------------------------------------------------
#                Face detection project
#--------------------------------------------------------
# File:         evaluation.py
# Author:       Quentin Loridant
# Date:         September 2016
# Description:  Function to evaluate the presence of a face in an image.
#               Requires: - the filename of the image
#
# The model files should be in the folder model (vgg16.npy, model-ckpt-0)

import os
import numpy as np
import tensorflow as tf 
import vgg16
from PIL import Image

class Model:

    """Model:

    - sess

    - image_input

    - y_

    -network"""

    # Create the model with the restored weigths and biais
    def __init__(self):

        # Connection with the backend
        self.sess = tf.InteractiveSession() 

        # Nodes for the input images (RGB with 224*224 resolution)
        self.image_input = tf.placeholder(tf.float32, [None, 224, 224, 3], name="input")
        # Target for the output classes
        self.y_ = tf.placeholder(tf.float32, shape=[None, 2], name="output")

        #  Calling the vgg16 architecture
        self.network = vgg16.Vgg16("model/vgg16.npy")
        self.y = tf.clip_by_value(self.network.build(self.image_input), 1e-15, 1)

        # Run tensorflow sessions
        self.sess.run(tf.initialize_all_variables())

        # Creating the saver object to save the model later
        saver = tf.train.Saver()
        print("[INFO] Restoring model")

        # Restoring the session with the model using the saver
        saver.restore(self.sess, "model/model.ckpt-0")
        print("[INFO] Model restored")

    # Tell if there is a face on an image or not 
    def evaluateImage(self, filename):
        # Fetching the image that was downloaded from the web
        image_batch = np.array([np.array(Image.open(filename).convert("RGB").resize([224,224]))])

        # Evaluation
        pred = self.sess.run([self.y], feed_dict={self.image_input:image_batch})

        print (pred[0])
        # No face was detected
        if (pred[0][0][0] > pred[0][0][1]):
            print("[RESULT] No face detected")
            return 0
        # A face has been detected
        else:
            print("[RESULT] Face detected")
            return 1

